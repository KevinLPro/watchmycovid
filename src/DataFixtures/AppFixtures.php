<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Tag;
use App\Entity\Recommendation;
use App\Entity\AssociationTagRecommandation;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $tag1 = new Tag();
        $tag1->setNom("Educatif");
        $manager->persist($tag1);



        $tag2 = new Tag();
        $tag2->setNom("Eveil");
        $manager->persist($tag2);



        $reco1 = new Recommendation();
        $reco1->setNom("Illusions Of Time");
        $reco1->setLien("https://www.youtube.com/watch?v=zHL9GP_B30E");
        $reco1->setDescription("Vidéo de Vsauce à propos des illusions temporelles");

        $asso1 = new AssociationTagRecommandation();
        $asso1->addTag($tag1);
        $asso1->addRecommandation($reco1);
        $manager->persist($asso1);

        $reco1->addAssociationTagRecommandation($asso1);
        $manager->persist($reco1);



        $reco2 = new Recommendation();
        $reco2->setNom("Wisdom Baby");
        $reco2->setLien("https://www.youtube.com/watch?v=N4Cz2CE0XhY");
        $reco2->setDescription("Wisdom baby shares his knowledge");

        $asso2 = new AssociationTagRecommandation();
        $asso2->addTag($tag2);
        $asso2->addRecommandation($reco2);
        $manager->persist($asso2);

        $reco2->addAssociationTagRecommandation($asso2);
        $manager->persist($reco2);


        
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
