<?php

namespace App\Entity;

use App\Repository\VideoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VideoRepository::class)
 */
class Video
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $idSalon;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lienVideo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $timeCode;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdSalon(): ?string
    {
        return $this->idSalon;
    }

    public function setIdSalon(?string $idSalon): self
    {
        $this->idSalon = $idSalon;

        return $this;
    }

    public function getLienVideo(): ?string
    {
        return $this->lienVideo;
    }

    public function setLienVideo(?string $lienVideo): self
    {
        $this->lienVideo = $lienVideo;

        return $this;
    }

    public function getTimeCode(): ?string
    {
        return $this->timeCode;
    }

    public function setTimeCode(?string $timeCode): self
    {
        $this->timeCode = $timeCode;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
