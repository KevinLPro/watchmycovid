<?php

namespace App\Entity;

use App\Repository\AssociationTagRecommandationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AssociationTagRecommandationRepository::class)
 */
class AssociationTagRecommandation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=Tag::class, inversedBy="associationTagRecommandations")
     */
    private $Tag;

    /**
     * @ORM\ManyToMany(targetEntity=Recommendation::class, inversedBy="associationTagRecommandations")
     */
    private $Recommandation;

    public function __construct()
    {
        $this->Tag = new ArrayCollection();
        $this->Recommandation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTag(): Collection
    {
        return $this->Tag;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->Tag->contains($tag)) {
            $this->Tag[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        $this->Tag->removeElement($tag);

        return $this;
    }

    /**
     * @return Collection|Recommendation[]
     */
    public function getRecommandation(): Collection
    {
        return $this->Recommandation;
    }

    public function addRecommandation(Recommendation $recommandation): self
    {
        if (!$this->Recommandation->contains($recommandation)) {
            $this->Recommandation[] = $recommandation;
        }

        return $this;
    }

    public function removeRecommandation(Recommendation $recommandation): self
    {
        $this->Recommandation->removeElement($recommandation);

        return $this;
    }
}
