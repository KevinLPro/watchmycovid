<?php

namespace App\Entity;

use App\Repository\SalonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SalonRepository::class)
 */
class Salon
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $CodeSalon;

    /**
     * @ORM\Column(type="datetime")
     */
    private $DateCreation;

    /**
     * @ORM\Column(type="datetime")
     */
    private $HeureDernierPing;

    /**
     * @ORM\Column(type="boolean")
     */
    private $EstSupprime;

    /**
     * @ORM\ManyToMany(targetEntity=AssociationUtilisateurSalon::class, mappedBy="Salon")
     */
    private $associationUtilisateurSalons;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Chef;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="Channel", orphanRemoval=true,cascade={"persist"})
     */
    private $messages;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $LienVideoEnCours;

    public function __construct()
    {
        $this->associationUtilisateurSalons = new ArrayCollection();
        $this->messages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeSalon(): ?int
    {
        return $this->CodeSalon;
    }

    public function setCodeSalon(int $CodeSalon): self
    {
        $this->CodeSalon = $CodeSalon;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->DateCreation;
    }

    public function setDateCreation(\DateTimeInterface $DateCreation): self
    {
        $this->DateCreation = $DateCreation;

        return $this;
    }

    public function getHeureDernierPing(): ?\DateTimeInterface
    {
        return $this->HeureDernierPing;
    }

    public function setHeureDernierPing(\DateTimeInterface $HeureDernierPing): self
    {
        $this->HeureDernierPing = $HeureDernierPing;

        return $this;
    }

    public function getEstSupprime(): ?bool
    {
        return $this->EstSupprime;
    }

    public function setEstSupprime(bool $EstSupprime): self
    {
        $this->EstSupprime = $EstSupprime;

        return $this;
    }

    /**
     * @return Collection|AssociationUtilisateurSalon[]
     */
    public function getAssociationUtilisateurSalons(): Collection
    {
        return $this->associationUtilisateurSalons;
    }

    public function addAssociationUtilisateurSalon(AssociationUtilisateurSalon $associationUtilisateurSalon): self
    {
        if (!$this->associationUtilisateurSalons->contains($associationUtilisateurSalon)) {
            $this->associationUtilisateurSalons[] = $associationUtilisateurSalon;
            $associationUtilisateurSalon->addSalon($this);
        }

        return $this;
    }

    public function removeAssociationUtilisateurSalon(AssociationUtilisateurSalon $associationUtilisateurSalon): self
    {
        if ($this->associationUtilisateurSalons->removeElement($associationUtilisateurSalon)) {
            $associationUtilisateurSalon->removeSalon($this);
        }

        return $this;
    }

    public function getChef(): ?string
    {
        return $this->Chef;
    }

    public function setChef(string $Chef): self
    {
        $this->Chef = $Chef;

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setChannel($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getChannel() === $this) {
                $message->setChannel(null);
            }
        }

        return $this;
    }

    public function getLienVideoEnCours(): ?string
    {
        return $this->LienVideoEnCours;
    }

    public function setLienVideoEnCours(string $LienVideoEnCours): self
    {
        $this->LienVideoEnCours = $LienVideoEnCours;

        return $this;
    }
}
