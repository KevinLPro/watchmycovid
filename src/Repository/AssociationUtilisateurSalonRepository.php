<?php

namespace App\Repository;

use App\Entity\AssociationUtilisateurSalon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AssociationUtilisateurSalon|null find($id, $lockMode = null, $lockVersion = null)
 * @method AssociationUtilisateurSalon|null findOneBy(array $criteria, array $orderBy = null)
 * @method AssociationUtilisateurSalon[]    findAll()
 * @method AssociationUtilisateurSalon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AssociationUtilisateurSalonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AssociationUtilisateurSalon::class);
    }

    // /**
    //  * @return AssociationUtilisateurSalon[] Returns an array of AssociationUtilisateurSalon objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AssociationUtilisateurSalon
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
